# FrontEnd CMU

```Requisitos: Python3; Pip3```

Instalar todas as dependências do Pip presentes no documento requirements
```pip3 install -r requirements.txt```
Executar o programa
```python3 http_server.py```
* Cada grupo fica com o "conteudo" de uma div para preencher.
* Podem criar utilizar tabelas, titulos, texto, etc. Idealmente o html já vem formatado do backend de cada grupo e é apenas aplicado ao template.
* Cada grupo deve só necessitar de alterar o método de código relativo ao seu grupo.
* O exemplo em comentário permite efetuar um pedido get, no entanto também é possível com a biblioteca request fazer pedidos do tipo post.


