#!/bin/sh
echo "+-+-+-+-+-+ Stop Docker Compose +-+-+-+-+-+"
docker-compose stop
echo "+-+-+-+-+-+ G1 Pull Image +-+-+-+-+-+"
docker pull filipeguimaraes99/amu-g1-idealista:latest
echo "+-+-+-+-+-+ G2 Pull Image +-+-+-+-+-+"
docker pull joaocadavezz/cmuubicompg2:latest
echo "+-+-+-+-+-+ Build Docker Compose +-+-+-+-+-+"
docker-compose build
echo "+-+-+-+-+-+ Docker Compose Up +-+-+-+-+-+"
docker-compose up -d
echo "+-+-+-+-+-+ Done! +-+-+-+-+-+"
